---
layout: page
title: "Disclaimer"
comments: false
sharing: false
footer: true
---

This blog contains articles I found and experiences using frameworks and Java in general, with a personal post mixed in between now and then.

The information in this weblog is provided “AS IS” with no warranties, and confers no rights. I do not accept liabililty should you use any code presented in my articles in production systems and something goes wrong. Consider all code presented on this site under an Apache 2.0 license.

This weblog does not represent the opinions, thoughts, intentions, plans or strategies of my employer. It is solely my opinion and I accept sole responsibility for the articles I write.