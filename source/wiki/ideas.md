# Techs

## CoreOS + Docker

https://coreos.com/

* Linux distributie waar Docker geïntegreerd is. Docker is enige manier om software te installeren
* Cluster ready out-of-the-box
* Lightweight


## Hystrix

https://github.com/Netflix/Hystrix

* Stabiliteit library voor Java
* Implementatie van veel patronen uit Release It! boek (circuit breaker, bulkheads, ...)

## Spring Boot (icm Docker: containerized Spring applications zonder appserver)

http://projects.spring.io/spring-boot/

* Embedded Tomcat/Jetty. Ook mogelijk om Undertow (JBoss) te gebruiken, maar nog niet 100% stabiel.

## Gradle Docker plugin

https://github.com/Transmode/gradle-docker

* Docker box maken op basis van deployment
* Heel interessant bij Spring Boot applicaties, installatie is Docker box starten

## Spring HATEOAS

https://spring.io/guides/gs/rest-hateoas/

* Discoverable REST services

## RAML/API BluePrint

* Contract voor REST services
* http://raml.org/
    * Support voor Java
    * Kan gebruikt worden in unit (Spock) tests
    * http://www.insaneprogramming.be/blog/2014/08/18/rest-documentation-specification/
* http://apiblueprint.org/
    * (nog) geen support voor Java

## RxJava

https://github.com/ReactiveX/RxJava

* Reactive library support voor Java (port van JavaScript). 
* Ook interessante extensions:
    * RxJava-jdbc: https://github.com/davidmoten/rxjava-jdbc
    * Queries via Observables

## Websockets or SSE (meer lightweight)

http://www.html5rocks.com/en/tutorials/eventsource/basics/
https://weblogs.java.net/blog/swchan2/archive/2014/05/21/server-sent-events-async-servlet-example

* Push ipv pull architectuur, interessant voor sommige use cases waar realtime updates handig kunnen zijn
* Oppassen met open HTTP connectie limit in Tomcat/Apache!

## Ansible

http://java.dzone.com/articles/how-ansible-and-docker-fit
http://www.ansible.com/

* Zoals Chef of Puppet, alleen iets meer streamlined / gemakkelijker
* Ansible Tower is heel interessant (betalend)

## PF4J

https://github.com/decebals/pf4j-spring

* Plugin-oriented development (poor man's OSGi)
* DDD applicatie: plugin per bounded context???

## GitBook

https://www.gitbook.io/

* Fantastisch voor documentatie te schrijven

## MockServer

http://www.mock-server.com/

* Snelle mock schrijven voor HTTP server (bijv. REST services)

## Vert.x voor non-blocking REST services

http://vertx.io

* Verticle per REST service
* Non blocking IO, dus heel snel
* Moeilijkere API dan pakweg Jersey of MVC voor REST services te schrijven, maar stukken performanter
* Wordt gebruikt door Malmberg (Sanoma Learning) voor alle REST communicatie
* Nadeel: apart platform (a la Node.JS)

## ElasticSearch als search backend voor database

http://www.elasticsearch.org

* Data queries scheiden van search queries
* ES data kan geoptimaliseerd worden voor search (duplicatie, denormalisatie)
* Efficiënter dan Hibernate Search
* JSON document oriented, heel gemakkelijk op te zetten met MongoDB
* Data funnels via 'rivers'
* RDBMS funnel vragen iets meer werk door relationeel vs embedded document problematiek, wel veel mogelijkheden met data joins en _id / _index
* Les bij De Persgroep: hardware aanpak is belangrijk -> horizontal scaling (veel cheap servers met SSD) veel beter dan vertical scaling (enkele krachtige servers) + when in doubt, call in the experts (op 1 dag vinden die de meeste setup issues)
* Expertise opbouwen + aanbieden?

## Faceted search met Solr/ElasticSearch

http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/search-facets.html
http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/search-aggregations.html
https://lucidworks.com/blog/faceted-search-with-solr/

* iets vinden ipv iets zoeken
* ElasticSearch + MongoDB river = faceted search op MongoDB database
* Terms aggregation
* Significant terms aggregation
* (Date) Range aggregation
    * Heel interessant in combinatie met Stats aggregation
* Geodistance aggregation (met 
* Geohash Grid aggregation (geo grouping)
* Shop voorbeeld: wat kopen mensen bij mij in de buurt
* Misdaad hotspots op basis van rapporten

## Activiti 5.15+

http://www.activiti.org

* Spring Boot support!
* Heeft RDBMS nodig om te draaien

## Logging using Logstash + Kibana + ElasticSearch

http://vaughndickson.com/2014/02/21/centralising-clojurejava-logging-with-logback-logstash-elasticsearch-and-kibana/

* Centralized logging en visualisatie/search
* Integratie met logback (dus ook SLF4J)

## Data Benerator

http://databene.org/databene-benerator.html

* Database data generation voor testing
* Production data anonymization 
* GPL licentie maar aangezien enkel voor interne unit testing niet echt een probleem

## MessagePack serializatie

http://msgpack.org/

* Kleinere footprint dan java serializatie
* Sneller
* Multi-lang clients
* Redis heeft built-in support (ideaal voor distributed caching dus)

## Animal Sniffer

* Checken of geen API's gebruikt worden die niet compatibel zijn met de target version van de JVM
    * Bijv. compileren met JDK 8 voor target 1.6 maar per ongeluk LocalDate gebruiken (geeft GEEN compile errors!)
* Plugin voor Gradle geschreven:
    * http://www.insaneprogramming.be/blog/2014/09/30/animal-sniffer-gradle/

    plugins {
        id 'be.insaneprogramming.gradle.animalsniffer' version '1.4.0'
    }
    ... 
    animalsniffer {
        signature = '...'
    }

## OpenShift Origin

https://www.openshift.com/products/origin

* deployable versie van JBoss OpenShift
* cartridges voor de stack binnen IIB
    * JBoss EAP
    * MySQL
    * MongoDB

## KeyCloak

http://keycloak.jboss.org/

* Nieuw SSO systeem voor JBoss EAP
* Nog geen support voor andere containers, volgende versie wss Tomcat support
* Geen out-of-the-box integratie met Spring Security (yet)

## SSO met Spring Security en CAS

http://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#cas

* Alternatief: OpenID (Google, Facebook, Twitter, GitHub, ...)
* Spring Security met 2-factor auth 
    * via OTP generator (Google Authenticator)
    * http://www.insaneprogramming.be/blog/2014/05/14/two-factor-otp-security/  
* Makkelijk te integreren met Spring Security authenticatie (InsufficientAuthenticationException)
* BeID integratie via Java library (app key laten signen met certificaat in eID en signed string gebruiken als (secundair) paswoord)
    * Moeilijk te testen (testkaarten moeilijk te verkrijgen en PITA om te unlocken bij 3 verkeerde pins)

## SaaS SSO service providers

* Stormpath
    * Integratie met Spring Security en Apache Shiro
    * Authorizatie via groups of custom data object (max. 10Mb)
    * SaaS of on-premise te installeren
    * Ondersteunt JSON web tokens
    * http://www.stormpath.com
* Auth0
    * Integratielibrary met Spring Security op github
    * https://github.com/calipho-sib/spring-security-auth0
    * SaaS en on-premise
    * Ondersteunt JSON web tokens
    * https://auth0.com/
    
## Hibernate OGM

http://hibernate.org/ogm/

* JPA persistentie naar NoSQL databases
* Support voor MongoDB!
* Binnenkort final

## Spring IO platform

http://platform.spring.io/platform/

* Combinatie van geteste versies van Spring
* Als je enkel de version management plugin wilt gebruiken: https://github.com/spring-gradle-plugins/dependency-management-plugin

## LazyBones

https://github.com/pledbrook/lazybones

* Archetypes voor Gradle projecten
* Documentatie over parameters in templates niet echt duidelijk (zie bestaande templates): gebruik van lazybones.groovy undocumented...
* Redelijk wat bestaande templates
* https://bintray.com/pledbrook/lazybones-templates
* Idee: eigen project intern maken met alle mogelijke nodige templates?

## IoT projects

* Johnny Five
    * NodeJS platform voor Arduino aan te spreken
    * Gebruikt Firmata sketch op Arduino voor communicatieprotocol
    * Kan niet overweg met seriële poorten aan te spreken
* libbulldog
    * Java library voor Raspberry Pi/BeagleBone Black
    * Volledige support voor alle features
* Hardware addons
    * Analoge reads op Raspberry Pi
        * MCP3008 gebruiken
        * http://www.adafruit.com/product/856
        * 10 bit resolutie. 12 bit is ook mogelijk: MCP3208
    * Meer analog/digital ports
        * Multiplexer breakout board
        * https://www.sparkfun.com/products/9056
        
## Presentaties met AsciiDoctor + RevealJS backend

* Vrij beperkt qua mogelijkheden, maar handig voor snelle presentaties
* https://github.com/asciidoctor/asciidoctor-reveal.js
* Public presentaties best gewoon zelf maken...

## MongoDB schema migration tools

* Zoals Flyway of Liquibase
* Mongeez
    * https://github.com/secondmarket/mongeez/wiki/How-to-use-mongeez
    * XML formaat
    * Moet runnen met admin user omwille van gebruik db.eval
* Mongobee
    * https://github.com/mongobee/
    * Pure Java (of Groovy) oplossing, geen XML's
    * Heeft enkel R/W user nodig, geen admin

## Easy software dev software install op pc's

* http://chocolatey.org
    * Bestaat ook tool die packages kan maken voor prebuild development machines
* http://boxstarter.org

## JSON Web Token voor authenticatie voor REST services

https://developer.atlassian.com/static/connect/docs/concepts/understanding-jwt.html

* Ideaal voor AngularJS applicaties
* ipv cookies!
* https://auth0.com/blog/2014/01/07/angularjs-authentication-with-cookies-vs-token/
* Integratie met Spring Security mogelijk
* http://javattitude.com/2014/06/07/spring-security-custom-token-based-rest-authentication/
* Mogelijk om hiermee simpele SSO mogelijk te maken

## Hikari CP voor connection pooling

http://brettwooldridge.github.io/HikariCP/

* Vervanging voor BoneCP, C3PO
* Veel performanter

## MongoDB runnen als embedded instance

https://github.com/jirutka/embedmongo-spring

* Super voor unit testing!
* Configureerbaar welke versie er gedraaid moet worden

## Gatling.io voor load testing ipv JMeter

http://gatling.io

* Gemakkelijker scripts te schrijven voor load/performance testing dan JMeter
* Feeders om inputdata aan te leveren (CSV, XML, JSON, database, ...)
* Heel goede rapportage
* Nadelen:
    * Minder geschikt voor UI's te testen, vooral goed voor REST services te testen
    * Geschreven in Scala, gebaseerd op Akka
        * API is vrij straightforward dus met basiskennis Scala kom je er wel
    * Onmogelijk om API te gebruiken zonder Scala, API gebruikt intern te veel Scala features (Option, actors, ...)
* Integreerbaar in Gradle
    * Beter om alle performance tests in apart project te steken

## Protractor

http://angular.github.io/protractor/#/

* End to end testing voor AngularJS apps

## Polymer

https://www.polymer-project.org/

* Web Components library
* extended tags voor elementen
* heel mooie look

## GORM gebruiken voor persistentie

http://grails.org/doc/latest/guide/GORM.html
https://spring.io/guides/gs/accessing-data-gorm-mongodb/

* DDD oriented, entiteiten hebben zelf gedrag om zichzelf te persisteren
* Heel goeie support in IntelliJ 14
* Kleine leercruve
* Support voor validatie
* Spring Boot support voor MongoDB en Hibernate 4